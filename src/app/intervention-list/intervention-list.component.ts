import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Intervention } from '../models/intervention.model';
import { InterventionService } from '../services/intervention.service';

@Component({
  selector: 'app-intervention-list',
  templateUrl: './intervention-list.component.html',
  styleUrls: ['./intervention-list.component.scss']
})
export class InterventionListComponent implements OnInit {
  interventions: Intervention[];

  displayedColumns: string[] = ['surgeon', 'specialty', 'nbr', 'anesthsiste', 'nurse1','roomNumber', 'intervention'];

  resultsLength = 0;
  isLoadingResults = true;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;


  pageSize = 10;
  page = 1;
  keyword = '';

  constructor(private interventionService: InterventionService) { }

  ngOnInit(): void {
    this.interventionService.findAll(this.keyword, this.page, this.pageSize).subscribe(res => {
      this.interventions = res.interventions;
            
      this.resultsLength = res.totalItems;
      
      this.listen();
    });



  }

  listen(){
        // If the user changes the sort order, reset back to the first page.
        this.paginator.page.subscribe(page =>{
          this.page = this.paginator.pageIndex + 1;
          this.pageSize = this.paginator.pageSize;

          console.log(this.paginator.length);

          this.interventionService.findAll(this.keyword, this.page, this.pageSize).subscribe(res => {
            console.log(res);
            this.interventions = res.interventions;
            this.table.renderRows();
          });
        });
  }
  applyFilter(event){
    console.log(event.target.value);
    this.keyword = event.target.value;
    this.interventionService.findAll(this.keyword, this.page, this.pageSize).subscribe(res => {
      console.log(res);
      this.interventions = res.interventions;
      this.table.renderRows();
    });

  }
}
