export class Intervention{
    surgeon: string;
    interventionsNumber: number;
    specialty: string;
    anesthsiste: string;
    nurse: string;
    roomNumber: number;
    intervention: string;

}