import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterventionListComponent } from './intervention-list/intervention-list.component';

const routes: Routes = [
  {path: 'interventions', component: InterventionListComponent},
  {path: '', redirectTo: '/interventions', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
