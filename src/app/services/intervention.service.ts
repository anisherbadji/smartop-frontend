import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InterventionService {

  url = environment.url +  '/api/interventions';
  constructor(private http: HttpClient) { }

  findAll(keyword: string, page: number, pageSize: number): Observable<any>{
    return this.http.get(this.url + '?keyword=' + keyword + '&page=' + page + '&size=' + pageSize);
  }
}
